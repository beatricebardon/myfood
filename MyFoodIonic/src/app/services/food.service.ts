import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private http: HttpClient, public storage: Storage) {
  }


  API_URL_PRODUIT = "https://fr.openfoodfacts.org/api/v0/product/"
  API_URL = " https://fr.openfoodfacts.org/"


  getAllAliments() {
    return this.http.get(`${this.API_URL}`)
  }

  getAlimentbyCodeBarre(codeBarre: number) {
    return this.http.get(`${this.API_URL_PRODUIT}${codeBarre}.json`)
  }
  // exemple code barre 8424657108919

  // private AlimentQueryList = [];
  // private index: number;

  // public updateAlimentQueryList(aliment: any) {
  //   this.AlimentQueryList.push(aliment);
  //   this.storage.set("AlimentQueryList", this.AlimentQueryList);
  // }

  // async getAlimentQueryList() {
  //   return this.storage.get("AlimentQueryList");
  // }

  // async videStorageQuery() {
  //   this.storage.remove("AlimentQueryList");
  // }

  // async videStorageIndex() {
  //   this.storage.remove("index");
  // }

  // public updateIndexQuery(index) {
  //   this.index = index;
  //   this.storage.set("index", this.index);
  // }

  // public getIndexQuery() {
  //   return this.storage.get("index");
  // }
}


