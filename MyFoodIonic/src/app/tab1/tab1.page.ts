import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FoodService } from '../services/food.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  index = 0;
  numeroPage = 0;
  indexQuery = 0;
  plusClicked: boolean = false;
  aliments: any[];
  codeBarre: any;

  constructor(
    //private router: Router,
    private foodService: FoodService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.aliments = [{
      "code": "3095756220014",
      "product": {
        "url": "https://fr.openfoodfacts.org/produit/3095756220014/fleuron-de-canard-fleury-michon",
        "image_front_small_url": "https://static.openfoodfacts.org/images/products/309/575/622/0014/front_fr.103.200.jpg",
        "ingredients_text_fr": "Foie maigre de canard, graisse de canard, _crème_ fraîche, _oeuf_ (ponte au sol),  fibre de chicorée, sel, sucre, poivre, antioxydant : ascorbate de sodium, conservateur : nitrite de sodium, colorant : lutéine",
        "product_name_fr": "Fleuron de Canard",
        "generic_name_fr": "Mousse de foie maigre de canard",
        "nutriments": {
          "energy_100g": "1890",
          "proteins_100g": "9",
          "fat_100g": "40",
          "carbohydrates_100g": "6.7",
          "sugars_100g": "0",
          "fiber_100g": "6.25",
          "salt_100g": "1.2",
        },
      }
    },
    {
      "code": "8424657108919",
      "product": {
        "url": "https://fr.openfoodfacts.org/produit/8424657108919/quinoa-figues-siken",
        "image_front_small_url": "https://static.openfoodfacts.org/images/products/842/465/710/8919/front_fr.4.400.jpg",
        "ingredients_text_fr": "sirop de riz*, graines (courge*, tournesol*, lin*, _sésame_*), flocons d'avoine sans gluten*, figues déshydratées* couvertes de farine de riz* 16%, graines de quinoa soufflé* 8%, huile de tournesol*",
        "product_name_fr": "Quinoa & figues",
        "generic_name_fr": "Barre biologique à base de quinoa et figues",
        "quantity": "40g",
        "nutriments": {
          "energy_100g": "1890",
          "proteins_100g": "9.5",
          "fat_100g": "21",
          "carbohydrates_100g": "52.5",
          "sugars_100g": "30",
          "fiber_100g": "6.25",
          "salt_100g": "0.075",
          "energy-kcal_value": "181",
        },
      }
    },
    {
      "code": "3095756220014",
      "product": {
        "url": "https://fr.openfoodfacts.org/produit/3095756220014/fleuron-de-canard-fleury-michon",
        "image_front_small_url": "https://static.openfoodfacts.org/images/products/309/575/622/0014/front_fr.103.200.jpg",
        "ingredients_text_fr": "Foie maigre de canard, graisse de canard, _crème_ fraîche, _oeuf_ (ponte au sol),  fibre de chicorée, sel, sucre, poivre, antioxydant : ascorbate de sodium, conservateur : nitrite de sodium, colorant : lutéine",
        "product_name_fr": "Fleuron de Canard",
        "generic_name_fr": "Mousse de foie maigre de canard",
        "nutriments": {
          "energy_100g": "1890",
          "proteins_100g": "9",
          "fat_100g": "40",
          "carbohydrates_100g": "6.7",
          "sugars_100g": "0",
          "fiber_100g": "6.25",
          "salt_100g": "1.2",
        },
      }
    },
    {
      "code": "3095756220014",
      "product": {
        "url": "https://fr.openfoodfacts.org/produit/3095756220014/fleuron-de-canard-fleury-michon",
        "image_front_small_url": "https://static.openfoodfacts.org/images/products/309/575/622/0014/front_fr.103.200.jpg",
        "ingredients_text_fr": "Foie maigre de canard, graisse de canard, _crème_ fraîche, _oeuf_ (ponte au sol),  fibre de chicorée, sel, sucre, poivre, antioxydant : ascorbate de sodium, conservateur : nitrite de sodium, colorant : lutéine",
        "product_name_fr": "Fleuron de Canard",
        "generic_name_fr": "Mousse de foie maigre de canard",
        "nutriments": {
          "energy_100g": "1890",
          "proteins_100g": "9",
          "fat_100g": "40",
          "carbohydrates_100g": "6.7",
          "sugars_100g": "0",
          "fiber_100g": "6.25",
          "salt_100g": "1.2",
        },
      }
    },
    {
      "code": "3095756220014",
      "product": {
        "url": "https://fr.openfoodfacts.org/produit/3095756220014/fleuron-de-canard-fleury-michon",
        "image_front_small_url": "https://static.openfoodfacts.org/images/products/309/575/622/0014/front_fr.103.200.jpg",
        "ingredients_text_fr": "Foie maigre de canard, graisse de canard, _crème_ fraîche, _oeuf_ (ponte au sol),  fibre de chicorée, sel, sucre, poivre, antioxydant : ascorbate de sodium, conservateur : nitrite de sodium, colorant : lutéine",
        "product_name_fr": "Fleuron de Canard",
        "generic_name_fr": "Mousse de foie maigre de canard",
        "nutriments": {
          "energy_100g": "1890",
          "proteins_100g": "9",
          "fat_100g": "40",
          "carbohydrates_100g": "6.7",
          "sugars_100g": "0",
          "fiber_100g": "6.25",
          "salt_100g": "1.2",
        },
      }
    },
    {
      "code": "3095756220014",
      "product": {
        "url": "https://fr.openfoodfacts.org/produit/3095756220014/fleuron-de-canard-fleury-michon",
        "image_front_small_url": "https://static.openfoodfacts.org/images/products/309/575/622/0014/front_fr.103.200.jpg",
        "ingredients_text_fr": "Foie maigre de canard, graisse de canard, _crème_ fraîche, _oeuf_ (ponte au sol),  fibre de chicorée, sel, sucre, poivre, antioxydant : ascorbate de sodium, conservateur : nitrite de sodium, colorant : lutéine",
        "product_name_fr": "Fleuron de Canard",
        "generic_name_fr": "Mousse de foie maigre de canard",
        "nutriments": {
          "energy_100g": "1890",
          "proteins_100g": "9",
          "fat_100g": "40",
          "carbohydrates_100g": "6.7",
          "sugars_100g": "0",
          "fiber_100g": "6.25",
          "salt_100g": "1.2",
        },
      }
    },
    {
      "code": "3095756220014",
      "product": {
        "url": "https://fr.openfoodfacts.org/produit/3095756220014/fleuron-de-canard-fleury-michon",
        "image_front_small_url": "https://static.openfoodfacts.org/images/products/309/575/622/0014/front_fr.103.200.jpg",
        "ingredients_text_fr": "Foie maigre de canard, graisse de canard, _crème_ fraîche, _oeuf_ (ponte au sol),  fibre de chicorée, sel, sucre, poivre, antioxydant : ascorbate de sodium, conservateur : nitrite de sodium, colorant : lutéine",
        "product_name_fr": "Fleuron de Canard",
        "generic_name_fr": "Mousse de foie maigre de canard",
        "nutriments": {
          "energy_100g": "1890",
          "proteins_100g": "9",
          "fat_100g": "40",
          "carbohydrates_100g": "6.7",
          "sugars_100g": "0",
          "fiber_100g": "6.25",
          "salt_100g": "1.2",
        },
      }
    }

    ]

    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    // this.getIndexQuery();
    this.getListeAliments();
    console.log(this.aliments)
  }

  getListeAliments() {
    this.foodService.getAllAliments().subscribe((response: any) => {
      this.aliments = response;
      this.aliments = Array.of(this.aliments);
      console.log(this.aliments);
    }, (error) => {
      console.log(error);
    })
  }
  getAlimentCodeBarre() {
    this.activatedRoute.paramMap.subscribe(params => { this.codeBarre = params.get('codeBarre'); });
    this.foodService.getAlimentbyCodeBarre(+this.codeBarre).subscribe((response: any) => {
      this.aliments = response;
      this.aliments = Array.of(this.aliments);
      console.log(response);
      return this.aliments;
    }, (error) => {
      console.log(error);
    })
  }


  // getIndexQuery() {
  //   this.foodService.getIndexQuery().then(res => {
  //     console.log(res);
  //     this.indexQuery = res;
  //   })
  // }

  currentAliment() {
    return this.aliments[this.index];
  }

  showPlusInfos() {
    this.plusClicked = !this.plusClicked;
  }

  alimentSuivant() {
    if (this.index > this.aliments.length) {
      //Si l'index correspond à la fin de la liste d'animaux ou la dépasse
      //=> revenir au début de la liste
      this.numeroPage = 1;
      this.index = 0;
    }
    else {
      //Sinon incrémente l'index
      this.numeroPage++;
      this.index++;
    }
  }

  alimentPrecedent() {
    if (this.index <= this.aliments.length) {
      this.numeroPage--;
      this.index--;
    }
  }
}

